<?php

class Logging{
    private $log_file = 'logfile.log';
    private $fp = null;
    public function lfile($path) {
        $this->log_file = $path;
    }
    public function lwrite($message){
        if (!$this->fp) $this->lopen();
        $script_name = pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME);
        $time = date('H:i:s');
        fwrite($this->fp, "$time ($script_name) $message\n");
    }
    private function lopen(){
        $lfile = $this->log_file;
        $today = date('Y-m-d');
        $this->fp = fopen($lfile . '_' . $today, 'a') or exit("Can't open $lfile!");
    }
}


include_once("lib.php");
include_once("xpathmapping.php");
include_once("passwd.php");

//$log = new Logging();
$updatevals = array();

session_start();

if (! isset($_SESSION["mydir"]) ) {

	die("need to login");

}

$dir = $metadataroot .$_SESSION["mydir"];


$error = false;
// do bulk updates first
if (isset($_REQUEST["contactsection_updateall"]) 
and $_REQUEST["contactsection_updateall"] == "on"  ) {

	//var_dump($_REQUEST);
	
	foreach ($_REQUEST as $key => $value) {
		
		if ( preg_match("/^update-/", $key ) ) { 

			$temp = explode("-",$key);
		
			$xpath = decodexpath($temp[2]);
			$prio = $temp[1];
			if ($prio == null) {
				$prio=0;
			}
			//$xpath = decodexpath(str_replace("update-","",$key));
		
			if (in_array($xpath,$contactsection)) {
			
				$updatevals[$prio+'-'+$xpath]=$value;
				//$log->lwrite("updating in all files: elem: $xpath, value $value");
			}
			
		}
	}
	
	// sort by key to make the updates in the right order, then remove priorities again
	ksort($updatevals);
	
	$temp=array();
	
	foreach ( $updatevals as $key => $val ) {
		$temp2= explode("-",$key);
		$temp[ $temp2[1] ] = $val;
	}
	
	$updatevals = $temp;
	
	$error = replacewithxpath($config_filemode,$dir,"*",$updatevals);
	
}	

// now do single update
$fileid = $_REQUEST["fileid"];
// $filenames = findfile($fileid,$dir);
$updatevals = array(); // reset updatevals
$deletevals = array(); // reset updatevals
$insertvals = array(); // reset insertvals


foreach ($_REQUEST as $key => $value) {


	if ( preg_match("/^update-/", $key ) ) { 
		
		
		$temp = explode("-",$key);
	
		$xpath = decodexpath($temp[2]);
		$prio = $temp[1];
		if ($prio == null) {
				$prio="0";
		}

		//$xpath = decodexpath(str_replace("update-","",$key));
		
		$mykey = $prio . "-" . $xpath;
		
		//print "found $filename for $fileid";
		$updatevals[$mykey]=$value;
		
		//$log->lwrite("updating $filename: elem: $xpath, value $value");
		
		
	}
	
	if ( preg_match("/^delete-/", $key ) ) { 
		
		$xpath = decodexpath(str_replace("delete-","",$key));
		$deletevals[$xpath]=$value;
	}
	
	if ( preg_match("/^insert-/", $key ) ) { 
		
		$xpath = decodexpath(str_replace("insert-","",$key));
		$insertvals[$xpath]=$value;
	}

}
		
ksort($updatevals);
	
$temp=array();
	
foreach ( $updatevals as $key => $val ) {
	$temp2= explode("-",$key);
	$temp[ $temp2[1] ] = $val;
}

$updatevals = $temp;

//var_dump($updatevals);	

// check if new filename is correct 

$errormessage="";
if (isset($updatevals['/gmd:MD_Metadata/gmd:fileIdentifier/gco:CharacterString'])) { 
	if ( !checkFilename($updatevals['/gmd:MD_Metadata/gmd:fileIdentifier/gco:CharacterString']) ) {
		$errormessage="filename ". $updatevals['/gmd:MD_Metadata/gmd:fileIdentifier/gco:CharacterString'] ."not correct";
	}
}

if ($errormessage == "") {
	$error2 = replacewithxpath($config_filemode,$dir,$fileid,$updatevals,$deletevals,$insertvals);
	$error = $error2 || $error ;
}
		
	
if (!$error) {
	
$page = $_SERVER['PHP_SELF']; 
$sec = "0"; 
header("Refresh: $sec; url=index.php?errormessage=$errormessage");
}
else {
	print "problem with updating";

}	


?>
