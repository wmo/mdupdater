#!/usr/bin/perl 

use DBI;
use File::Basename;

my $mdfiles = "/";
our $dbh = DBI->connect('DBI:mysql:mdupdater:localhost',
                       'mdupdater',  # user name
                       'mdupdater',  # password
                       { RaiseError => 1 });

my $sth= $dbh->prepare( "INSERT INTO metadatafiles(filenameIdentifier,metadata,lastchanged,country)".  " VALUES ( ?,?,NOW(),?) ");


chdir("/srv/www/htdocs/mdupdate/metadata/");

foreach my $directory ( <*> ) {


	foreach my $mdfile ( <$directory/*> ) {

		my $path = "$mdfile";

		open(my $fh, $path  ) or die $! . " $path";
		read( $fh, $var, -s $fh );


		my ($name,$dir,$ext) = fileparse($path,'\.xml');
		
		print "doing $name - $directory\n";

		$sth->execute($name,$var,$directory);


		print "$directory/$mdfile" . "\n";
	}


}

$sth->finish;
$dbh->disconnect;
