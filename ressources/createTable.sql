CREATE TABLE metadatafiles (
  filenameIdentifier VARCHAR(100),
  metadata BLOB,
  lastchanged TIMESTAMP(8),
  country varchar(10)
);
