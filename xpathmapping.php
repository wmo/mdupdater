<?php

$md = "/gmd:MD_Metadata";
$dataid = "$md/gmd:identificationInfo/gmd:MD_DataIdentification";
$respparty = "$md/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:pointOfContact/gmd:CI_ResponsibleParty";
$resppartycon = "$respparty/gmd:contactInfo/gmd:CI_Contact";
$extent = "$dataid/gmd:extent/gmd:EX_Extent/gmd:geographicElement/gmd:EX_GeographicBoundingBox";
$desckeywords = "$dataid/gmd:descriptiveKeywords";
$relativ_thesaurus = "gmd:MD_Keywords/gmd:thesaurusName/gmd:CI_Citation/gmd:title/gco:CharacterString";
$relativ_thes_keyword = "gmd:MD_Keywords/gmd:keyword";
$distrib = "$md/gmd:distributionInfo/gmd:MD_Distribution";
$distributor = "$distrib/gmd:distributor/gmd:MD_Distributor/gmd:distributorContact/gmd:CI_ResponsibleParty";
$distributorcon = "$distributor/gmd:contactInfo/gmd:CI_Contact";
$distributionformat = "$distrib/gmd:distributionFormat/gmd:MD_Format";
$mdcontact = "$md/gmd:contact/gmd:CI_ResponsibleParty";
$mdcontactcon = "$mdcontact/gmd:contactInfo/gmd:CI_Contact";

$filenamesection = array(
array( "name" => "fileid" , "xpath" => "$md/gmd:fileIdentifier/gco:CharacterString"),
array( "name" => "Title" , "xpath" => "$dataid/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString"),
array( "name" => "Abstract" , "xpath" => "$dataid/gmd:abstract/gco:CharacterString"),
array( "name" => "MD language" , "xpath" => "$dataid/gmd:language/gmd:LanguageCode",
"type" => "dropdown", "values" => array("eng","de","fr","bg") , "id" => "mdinfolanguage" )
);

$constraintsection = array(
array( "name" => "GTS category", "xpath"  => "$dataid/gmd:resourceConstraints/gmd:MD_LegalConstraints/gmd:otherConstraints[1]/gco:CharacterString" , 
"type" => "dropdown", "values" => array("WMOEssential","WMOAdditional","WMOOther","other","none"), "id" => "constraintdatapolicy" ),
array( "name" => "GTS priority", "xpath"  => "$dataid/gmd:resourceConstraints/gmd:MD_LegalConstraints/gmd:otherConstraints[2]/gco:CharacterString" ,
"type" => "dropdown", "values" => array("GTSPriority1","GTSPriority2","GTSPriority3","GTSPriority4") , "id" => "constraintpriority" )
);

$mdcontactsection = array( // /gmd:MD_Metadata/gmd:identificationInfo[1]/gmd:MD_DataIdentification[1]/gmd:pointOfContact[1]/gmd:CI_ResponsibleParty[1]/gmd:phone[1]
array( "name" => "Name" ,  "xpath" => "$mdcontact/gmd:organisationName/gco:CharacterString"),
array( "name" => "Tel" , "prio" => "2", "xpath" => "$mdcontactcon/gmd:phone/gmd:CI_Telephone/gmd:voice/gco:CharacterString"),
array( "name" => "Fax" , "prio" => "1", "xpath" => "$mdcontactcon/gmd:phone/gmd:CI_Telephone/gmd:facsimile/gco:CharacterString"),
array( "name" => "Address" , "xpath" => "$mdcontactcon/gmd:address/gmd:CI_Address/gmd:deliveryPoint/gco:CharacterString"),
array( "name" => "City" , "xpath" => "$mdcontactcon/gmd:address/gmd:CI_Address/gmd:city/gco:CharacterString"),
array( "name" => "Postal Code", "xpath"  => "$mdcontactcon/gmd:address/gmd:CI_Address/gmd:postalCode/gco:CharacterString"),
array( "name" => "Country" , "xpath" => "$mdcontactcon/gmd:address/gmd:CI_Address/gmd:country/gco:CharacterString"),
array( "name" => "Email" , "xpath" => "$mdcontactcon/gmd:address/gmd:CI_Address/gmd:electronicMailAddress/gco:CharacterString"),
array( "name" => "Web" , "xpath" => "$mdcontactcon/gmd:onlineResource/gmd:CI_OnlineResource/gmd:linkage/gmd:URL")
);

$contactsection = array(
array( "name" => "Name" , "xpath" => "$respparty/gmd:organisationName/gco:CharacterString"),
array( "name" => "Tel" , "prio" => "2", "xpath" => "$resppartycon/gmd:phone/gmd:CI_Telephone/gmd:voice/gco:CharacterString"),
array( "name" => "Fax" , "prio" => "1", "xpath" => "$resppartycon/gmd:phone/gmd:CI_Telephone/gmd:facsimile/gco:CharacterString"),
array( "name" => "Address" , "xpath" => "$resppartycon/gmd:address/gmd:CI_Address/gmd:deliveryPoint/gco:CharacterString"),
array( "name" => "City" , "xpath" => "$resppartycon/gmd:address/gmd:CI_Address/gmd:city/gco:CharacterString"),
array( "name" => "Postal Code", "xpath"  => "$resppartycon/gmd:address/gmd:CI_Address/gmd:postalCode/gco:CharacterString"),
array( "name" => "Country" , "xpath" => "$resppartycon/gmd:address/gmd:CI_Address/gmd:country/gco:CharacterString"),
array( "name" => "Email" , "xpath" => "$resppartycon/gmd:address/gmd:CI_Address/gmd:electronicMailAddress/gco:CharacterString"),
array( "name" => "Web" , "xpath" => "$resppartycon/gmd:onlineResource/gmd:CI_OnlineResource/gmd:linkage/gmd:URL")
);

$distributorsection = array(
array( "name" => "Person" , "xpath" => "$distributor/gmd:individualName/gco:CharacterString"),
array( "name" => "Name" , "xpath" => "$distributor/gmd:organisationName/gco:CharacterString"),
array( "name" => "Tel" , "xpath" => "$distributorcon/gmd:phone/gmd:CI_Telephone/gmd:voice/gco:CharacterString"),
array( "name" => "Fax" , "xpath" => "$distributorcon/gmd:phone/gmd:CI_Telephone/gmd:facsimile/gco:CharacterString"),
array( "name" => "Address" , "xpath" => "$distributorcon/gmd:address/gmd:CI_Address/gmd:deliveryPoint/gco:CharacterString"),
array( "name" => "City" , "xpath" => "$distributorcon/gmd:address/gmd:CI_Address/gmd:city/gco:CharacterString"),
array( "name" => "Postal Code", "xpath" => "$distributorcon/gmd:address/gmd:CI_Address/gmd:postalCode/gco:CharacterString"),
array( "name" => "Country" , "xpath" => "$distributorcon/gmd:address/gmd:CI_Address/gmd:country/gco:CharacterString"),
array( "name" => "Email" , "xpath" => "$distributorcon/gmd:address/gmd:CI_Address/gmd:electronicMailAddress/gco:CharacterString"),
array( "name" => "Web" , "xpath" => "$distributorcon/gmd:onlineResource/gmd:CI_OnlineResource/gmd:linkage/gmd:URL"),
array( "name" => "Format Name" , "xpath"  => "$distributionformat/gmd:name/gco:CharacterString"),
array( "name" => "Format Version" , "xpath" => "$distributionformat/gmd:version/gco:CharacterString"),
array( "name" => "Format Specification" , "xpath" => "$distributionformat/gmd:specification/gco:CharacterString"),
array( 
	"name" => "distribution URL" , 
	"xpath" => "$distrib/gmd:distributor/gmd:MD_Distributor/gmd:distributorTransferOptions/gmd:MD_DigitalTransferOptions/gmd:onLine/gmd:CI_OnlineResource/gmd:linkage/gmd:URL" , 
	"validation" => "url: true"  )
);


$extentsection = array(
array( "name" => "west" , "xpath" => "$extent/gmd:westBoundLongitude/gco:Decimal"  , "validation" => "range: [0,180]" ),
array( "name" =>  "east" , "xpath" => "$extent/gmd:eastBoundLongitude/gco:Decimal" , "validation" => "range: [0,180]"),
array( "name" =>  "north" , "xpath" => "$extent/gmd:northBoundLatitude/gco:Decimal" , "validation" => "range: [0,90]"),
array( "name" =>  "south" , "xpath" => "$extent/gmd:southBoundLatitude/gco:Decimal" , "validation" => "range: [0,90]")
);

$xpathmapping = array_merge($filenamesection,$constraintsection,$contactsection,$extentsection,$distributorsection,$mdcontactsection);

?>
