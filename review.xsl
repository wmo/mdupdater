<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
		xmlns:gmd="http://www.isotc211.org/2005/gmd" 
		xmlns:gmx="http://www.isotc211.org/2005/gmx" 
		xmlns:gco="http://www.isotc211.org/2005/gco" 
		xmlns:xlink="http://www.w3.org/1999/xlink" 
		xmlns:gts="http://www.isotc211.org/2005/gts" 
		xmlns:gml="http://www.opengis.net/gml/3.2" 
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		>

	<xsl:output
			method="html"
			omit-xml-declaration="yes"
			doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
			doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
			indent="yes"/>

	<xsl:template match="/gmd:MD_Metadata">


		<html>
			<head>

				<script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0"></script>



		
				<script type="text/javascript" src="js/jquery.js"></script>
				<script type="text/javascript" src="js/custom.js"></script>
				<script type="text/javascript" src="js/lib.js"></script>
					
				<link rel="stylesheet" type="text/css" href="css/css.css" />
			</head>

			<body>

				<div class="infobox">
					<h1>WMO WIS metadataeditor</h1>
					<p>Please review the information below. You can open a section by clicking on the "toggle view" link. Clicking on "edit" enables the edit fields. Some sections, such as the contact one, are configured to allow updating all the files in the directory with the information input.
					You can download the original file by clicking on the download link.
					</p>
				</div>

				<form method="POST" action="update.php">
					<input type="hidden" name="fileid" > 
						<xsl:attribute name="value">
							<xsl:value-of select="{$fileid}"/>
						</xsl:attribute>
					</input>



					<p><a href="?mode=list">go to parent directory</a></p>
					<p><a>
						<xsl:attribute name="href">
							<xsl:value-of select="concat('?mode=downloadfile&amp;fileid=',{$fileid})"/>
						</xsl:attribute>
						download file
					</a></p>
					

					<h2>General Info:<a  class="trigger" href="#">toggle view</a><a class="triggeredit open_beginning" href="#">edit</a></h2>
					<div class="toggle_container open_beginning">
						<div class="block">

							<table class="fields">
								{foreach from=$filenamesection key=k item=v}
								<tr class="valrow">
									<td>
										<span class="keyword"> {$k}:</span>
										<span xpath="{$v}" class="fieldvalue">
											<xsl:value-of select="{$v}"/>
										</span>
									</td>

									<td>

										<span class="editform">
										</span>
									</td>
								</tr>
								{/foreach}

							</table>

						</div>
					</div>


					<h2>Bulletin constraints<a  class="trigger" href="#">toggle view</a><a class="triggeredit" href="#">edit</a></h2>
					<div class="toggle_container">
						<div class="block">

							<table class="fields">
								{foreach from=$constraintsection key=k item=v}
								<tr class="valrow">
									<td>
										<span class="keyword"> {$k}:</span>
										<span xpath="{$v}" class="fieldvalue">
											<xsl:value-of select="{$v}"/>
										</span>


									</td>

									<td>
										<span class="editform">
										</span>
									</td>
								</tr>
								{/foreach}

							</table>
						</div>
					</div>

					<h2>Geographical extent<a  class="trigger" href="#">toggle view</a><a class="triggeredit geoclass" href="#">edit</a></h2>
					<div class="toggle_container">
						<div class="block">


							<table>
								<tr >

									<td>
										<table class="fields">
											{foreach from=$extentsection key=k item=v}
											<tr class="valrow">
												<td>
													<span class="keyword"> {$k}:</span>
													<span id="{$k}" xpath="{$v}" class="fieldvalue">
														<xsl:value-of select="{$v}"/>
													</span>
												</td>

												<td>

													<span class="editform">
													</span>
												</td>
											</tr>
											{/foreach}

										</table>
									</td>
									<td>

										<div id='mapDiv' style="position:relative; width:400px; height:400px; margin-left:60px;"></div>       

									</td>
								</tr>
							</table>
						</div>
					</div>


					<h2>Keywords <a  class="trigger" href="#">toggle view</a><a class=" triggereditdelete triggeredit" href="#">edit</a></h2>
					<div class="toggle_container">
						<div class="block">


							<xsl:for-each select="{$desckeywords}">
								<xsl:variable name="i" select="position()"/>

								<table  class="fields">
									<tr><th colspan="2">
											<p class="thesaurustitle">
													<xsl:attribute name="xpath">
														<xsl:value-of select="concat('{$desckeywords}[',$i,']')"/>
													</xsl:attribute>
												<xsl:value-of select="{$relativ_thesaurus}">
												</xsl:value-of>

											</p>
										</th></tr>


									<xsl:for-each select="{$relativ_thes_keyword}">
										<xsl:variable name="j" select="position()"/>

										<tr class="valrow">
											
											<td width="250px;">
												<span></span>
												<span class="fieldvalue">
													<xsl:attribute name="xpath">
														<xsl:value-of select="concat('{$desckeywords}[',$i,']/{$relativ_thes_keyword}[',$j,']/gco:CharacterString')"/>
													</xsl:attribute>

													<xsl:value-of select="gco:CharacterString"/>
												</span>
											</td>

											<td ><span class="editform"></span>
											</td>

										</tr>

									</xsl:for-each>


								</table>

							</xsl:for-each>





						</div>
					</div>



					<h2>Contact details <a  class="trigger" href="#">toggle view</a><a class="triggeredit" href="#">edit</a></h2>
					<div class="toggle_container">
						<div class="block">
							<span class="updateall">Update in all files: <input type="checkbox" class="updateallbox" name="contactsection_updateall" /> </span>

							<table class="fields">
								{foreach from=$contactsection key=k item=v}
								<tr class="valrow">
									<td>
										<span class="keyword"> {$k}:</span>
										<span xpath="{$v}" class="fieldvalue">
											<xsl:value-of select="{$v}"/>
										</span>
									</td>

									<td>

										<span class="editform">
										</span>
									</td>
								</tr>
								{/foreach}

							</table>
						</div>
					</div>
					
							<h2>Distributor details and format<a  class="trigger" href="#">toggle view</a><a class="triggeredit" href="#">edit</a></h2>
					<div class="toggle_container">
						<div class="block">
							<span class="updateall">Update in all files: <input type="checkbox" class="updateallbox" name="distributorsection_updateall" /> </span>

							<table class="fields">
								{foreach from=$distributorsection key=k item=v}
								<tr class="valrow">
									<td>
										<span class="keyword"> {$k}:</span>
										<span xpath="{$v}" class="fieldvalue">
											<xsl:value-of select="{$v}"/>
										</span>
									</td>

									<td>

										<span class="editform">
										</span>
									</td>
								</tr>
								{/foreach}

							</table>
						</div>
					</div>
					
					
					


					<input style="margin-top:20px;" type="submit" value="update record"/>
				</form>

			</body></html>

	</xsl:template>




</xsl:stylesheet>
