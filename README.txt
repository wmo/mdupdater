This document contains a short description of the architecture of the MD updater.
The current version is 1.0.

The md updater makes use of a templating engine (smarty) and xsl transformations, as well as java script 
to create a flexible webfrontend to edit ISO19139 files. It assumes that the XML files that are edited
with it have all the same structure and is thus not suitable for random XML/ISO19139 file editing.
However, it is easily customizable, and can be made to work with any form of XML/ISO19139 format.
The MDupdater can work with metadata files either in the filesystem or a database.

Structure of the programm
./metadata contains the metadata files, in subdirectories, configured in passwd.php
./passwd.php contains usernames and passwords as well as subdirectories in ./metadata that are made editable.
./mdtemplates contains metadata templates that are used by the MDupdater to create new MD records.
./index.php contains the main programm logic
./update.php handles the updates of metadata files
./libs contains the smarty templating engine
./lib.php contains functions used by index.php, to load and modify metadata files.
./review.xls.php outputs the xsl transformation needed to transform a MD file into a html page for viewing.
./template_smarty contains the templates for the login, the listfiles and the metadata view pages.
./js contains the java script needed to make the html form containing the MD information editable.

1) index.php
The main functionality of the programm is implemented in index.php.
Here the authentification, and the dispatching of the different actions (login,logout,create template, list files, view file etc.)
is treated. For the login and the listfile actions, smarty templates are loaded from the smarty template directory.
The "getfile" action transforms the metadata record into a sophisticated html page using a xsl transformation.
The resulting html page contains java script that makes selected fields editable by turning them into a HTML form.
The name of the form fields are the xpath expressions needed to write the changed value back into the XML.
The XSL transformation stylesheet itself it generated using smarty.

2) temaplate_smarty/review.xsl 
This transformation turns a XML document into a html document. The file is a smarty template and needs to be passed
several arguments. This happens in the function outputxsl() in lib.php, which passes the arguments and then triggers 
the smarty and also outputs the finished result, a xsl stylesheet. 
The template consists of several sections. Each section expects an array of arrays, containing "name" and "xpath" values.
The template then generates a list of fields looping over the arrays contained in the first array.
The name comes from the "name" value, the xpath expression is used both to name the form field and to extract later (in the xslt)
the value from the XML.
The template defines smarty functions (generatefields,generatefieldscontact), that contain the xsl code that is common to most sections. These functions are called
Some sections (keywords, geographical extent) are different though, and do not used these functions.

3) update.php 
Can both do mass and single updates. Loads the XML file in question into a DOM and then modifies the values of nodes
using the xpath expressions that are passed as part of the http request. The heavy lifting of the DOM update is done in lib.php.
The mass update is only triggered for sertain sections.


4) xpathmapping.php
Contains the xpath expressions and names of the fields that should be editable.
For convenience, some frequently used xpaths are defined as php variables in the beginning.
The html form is structured into several sections. Each section contains a number of fields.
Each field has a name and an xpath. This grouping is done in xpathmapping.php.
It can also be determined that the html form field should be a dropdown by indicating this here. (see language)

5) js/custom.js
Contains the java script code that turns the html page into a html form. It has differents sections for handling the
different requirements of normal fiedls, keyword fields etc.


what to do when changing the code.

If one wants to adapt the xpath that leads to a certain field, all one needs to do is to change the xpath in xpathmapping.php .
If a new field should be added, it is sufficient to add it to one of the existing sections.
If a new section should be added, several things need to be done.
First, the new section needs to be created in xpathmapping.php. 
Then, this section needs to be passed into the smarty template in outputxsl() in lib.php. 
Finally, in the smarty template itself, (review.xsl), a new section needs to be added.
If the section is similar to existing ones, the smarty template functions generatefields or generatefieldscontact can simply
be called to insert the right code.


To change the looks of the programm, one can adapt login.html, listfiles.html and review.xsl in the smarty templates directory.
IMPORTANT. The java script that turns the html page into a html form, assumes a certains structure, the template can thus not
be changed at will. 


