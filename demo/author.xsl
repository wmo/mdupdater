<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xalan="http://xml.apache.org/xslt" xmlns="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xalan">
  <xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes" omit-xml-declaration="yes"
      doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
      doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
      xalan:indent-amount="2"/>
  <xsl:strip-space elements="books"/>

  <xsl:include href="amazon.xsl"/>
  <xsl:include href="google.xsl"/>

  <!-- Create a table of authors and titles based on the title -->
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
      <head>
        <title>Authors</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style>
          .title { float:left; text-align:center; border:1px solid black; margin: 1em; padding: 1em; width: 12em; background-color:#eee } 
          .title p { text-align:center }
        </style>
      </head>
      <body>
        <xsl:apply-templates/>
      </body>
    </html>
  </xsl:template>

  <!-- Matches a <title> element in the input document -->
  <xsl:template match="title">

    <!-- Get the author of the title -->
    <xsl:variable name="author">
      <xsl:call-template name="get-author">
        <xsl:with-param name="title" select="."/>
      </xsl:call-template>
    </xsl:variable>
    
    <!-- Get an image of the book -->    
    <xsl:variable name="image">
      <xsl:call-template name="get-image">
        <xsl:with-param name="title" select="."/>
      </xsl:call-template>
    </xsl:variable>

    <!-- Get a url based on the author's name -->
    <xsl:variable name="url">
      <xsl:call-template name="feeling-lucky">
        <xsl:with-param name="search-term" select="$author"/>
      </xsl:call-template>
    </xsl:variable>

    <!-- Display the author name and link -->
    <div class="title">
      <p>
        <img>
          <xsl:attribute name="src">
            <xsl:value-of select="$image"/>
          </xsl:attribute>
        </img>
      </p>
      <p>
        <td><xsl:value-of select="."/></td>
      </p>
      <p>
        <a>
          <xsl:attribute name="href">
            <xsl:value-of select="$url"/>
          </xsl:attribute>
          <xsl:value-of select="$author"/>
        </a>
      </p>
    </div>
  </xsl:template>

</xsl:stylesheet>
