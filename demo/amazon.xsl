<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <!-- include reference to document containing the developer's keys -->
  <xsl:import href="keys.xsl"/>

  <xsl:variable name="amazon">http://xml.amazon.com/onca/xml?v=1.0&amp;t=webservices-20&amp;mode=books&amp;type=lite&amp;page=1&amp;f=xml</xsl:variable>

  <xsl:template name="get-author">
    <xsl:param name="title"/>

    <xsl:variable name="amazon-title"  select="translate(normalize-space($title), ' ', '+')"/>
    <xsl:variable name="amazon-key"    select="document($key)//amazon"/>
    <xsl:variable name="amazon-search" select="concat($amazon, '&amp;KeywordSearch=', $amazon-title, '&amp;dev-t=', $amazon-key)"/>
    <xsl:variable name="amazon-result" select="document($amazon-search)"/>

    <xsl:value-of select="($amazon-result//Authors)[1]/Author[1]"/>

  </xsl:template>
  
  <xsl:template name="get-image">
    <xsl:param name="title"/>

    <xsl:variable name="amazon-title"  select="translate(normalize-space($title), ' ', '+')"/>
    <xsl:variable name="amazon-key"    select="document($key)//amazon"/>
    <xsl:variable name="amazon-search" select="concat($amazon, '&amp;KeywordSearch=', $amazon-title, '&amp;dev-t=', $amazon-key)"/>
    <xsl:variable name="amazon-result" select="document($amazon-search)"/>

    <xsl:value-of select="$amazon-result//ImageUrlMedium"/>

  </xsl:template>


</xsl:stylesheet>