<?php

function checkFilename($filename) {

	// urn:x-wmo:md:int.wmo.wis::FTIQ20ORBS.xml
	
	$parts = explode(':',$filename);
	
	if ( count($parts) > 5 && $parts[0] == "urn" && $parts[1] == "x-wmo" && $parts[2] == "md" ) {
		return true ; /// preg_match('^(?:[-A-Za-z0-9]+\.)+[A-Za-z]{2,6}$', $parts[3]); 
	}
	else {
		return false;
	}

}

function getCon() {

	global $config_host,$config_username,$config_password,$config_db;

	if (!mysql_connect($config_host, $config_username, $config_password))
    		die("Can't connect to database");

	if (!mysql_select_db($config_db))
    		die("Can't select database");

}



function putfile($mode,$dir,$filename,$content) {

	if ($mode != "db" ) {

		$file = fopen("$dir/$filename", "w");
		fwrite($file, $content);
		fclose ($file);  
	
	}
	else {
		getcon();
		$content = mysql_real_escape_string($content);
		$filename = mysql_real_escape_string($filename);
		$country = basename($dir);

                mysql_query("DELETE from metadatafiles WHERE  filenameIdentifier='$filename' AND country='$country'");
                mysql_query("INSERT INTO metadatafiles SET filenameIdentifier='$filename', country='$country', metadata='$content', lastchanged=NOW()");

	}


}

function deletefiles($mode,$dir,$files) {

	if (! is_array($files) ) {
		$files = array($files);
	}

	$ret = array();
	
	if ($mode != "db") {
		
		foreach ($files as $file) {
			if (!unlink($dir."/".$file)) {
				$ret[$file] = "problem with $file";
			} 
		}
		
	}
	
	return $ret;
	
}

function getfiles($mode,$dir) {

	$ret = array();


	if ($mode != "db" ) {
	
		if ($handle = opendir($dir)) {
    			while (false !== ($file = readdir($handle))) {
        			if ($file != "." && $file != ".." && !(strpos($file,".xml") === false) ) {
                        		array_push($ret,$file);
        			}
    			}
    			closedir($handle);
		}
	}
	else {
		getcon();
		$country = basename($dir);
		$sql = "SELECT filenameIdentifier FROM metadatafiles where country='$country'";
		$result = mysql_query($sql);

		while ( $row = mysql_fetch_array($result) ){ 
			array_push($ret,$row["filenameIdentifier"]);
		}
	}


	return $ret;
}

function getfile($mode,$dir,$fileid) {
	if ($mode != "db" ) {
		        #$filenames = findfile($fileid,$dir);
		        $filename = "$dir/$fileid";
	
			$fh = fopen($filename, 'r');
			$theData = fread($fh, filesize($filename));
			fclose($fh);
			return $theData;
	}
	else {
		getcon();
		$fileid = mysql_real_escape_string($fileid);
		$country = basename($dir);
		$result = mysql_query("SELECT metadata FROM metadatafiles where country='".$country."' AND filenameIdentifier='".$fileid."' ORDER BY filenameIdentifier");

		if (mysql_num_rows($result) == 0)
        		die("no file with $fileid");

		$row = mysql_fetch_row($result);

		return $row[0];
	}
}

function outputxsl() {
	
	include("xpathmapping.php");
	define('SMARTY_DIR', './libs/');
	require_once(SMARTY_DIR . 'Smarty.class.php');
	$smarty = new Smarty();

	// transmit xpaths to template
	foreach($xpathmapping as $key => $value) {
		$smarty->assign($value["name"],$value["xpath"]); 
	}

	// check which ones have validation rules
	$validations = array();
	foreach($xpathmapping as $key => $value) {
		if (isset($value["validation"]) ) {
			$temp = explode(':', $value["validation"]);
			if  (count($temp) != 2 ) { continue; }
			$validations[] = array ( "type" =>  $temp[0] , "value" => $temp[1] , "fieldname" => "update--".$value["xpath"] );
		}
	}
	
	$smarty->assign("validationrules",$validations); 


	// assign 
	
	$smarty->assign("filenamesection",$filenamesection);
	$smarty->assign("constraintsection",$constraintsection);
	$smarty->assign("contactsection",$contactsection);
	$smarty->assign("extentsection",$extentsection);
	$smarty->assign("distributorsection",$distributorsection);
	$smarty->assign("mdcontactsection",$mdcontactsection);
	
	
	$smarty->assign("desckeywords",$desckeywords );
	$smarty->assign("relativ_thesaurus",$relativ_thesaurus );
	$smarty->assign("relativ_thes_keyword",$relativ_thes_keyword);	
	
	
	return @$smarty->fetch('template_smarty/review.xsl');

}

function genRandomString() {
    $length = 10;
    $characters = �0123456789abcdefghijklmnopqrstuvwxyz�;
    $string = �;    

    for ($p = 0; $p < $length; $p++) {
        $string .= $characters[mt_rand(0, strlen($characters))];
    }

    return $string;
}

function findfile($fileid,$mddir) {

	if ( $fileid != "*" ) {
		$split = explode(":",$fileid);
		$gtsid = $split[5];
	}
	
	$filenames = array();

	if ($handle = opendir($mddir)) {
		while (false !== ($file = readdir($handle))) {
			if ($file != "." && $file != ".." && preg_match("/.xml$/i", $file ) ) {
				
				if ($fileid == "*") {
					array_push($filenames,"$mddir/$file");
					continue;
				}
			
				if ( preg_match("/$gtsid/i", $file ) ) {
					array_push($filenames,$mddir."/".$file);
					break;
				}
			}
		}
		closedir($handle);
	}
	
	return $filenames;

}

function decodexpath($string) {

	return str_replace("%5b","[", str_replace("%5d","]",$string));

}


function getXpath($dom) {

	$xpath = new DOMXPath($dom);
	$xpath->registerNamespace('gmd', "http://www.isotc211.org/2005/gmd");
	$xpath->registerNamespace('gmx', "http://www.isotc211.org/2005/gmx");
	$xpath->registerNamespace('gco', "http://www.isotc211.org/2005/gco");
	$xpath->registerNamespace('gco', "http://www.w3.org/1999/xlink");
	$xpath->registerNamespace('gts', "http://www.isotc211.org/2005/gts");
	$xpath->registerNamespace('gml', "http://www.opengis.net/gml/3.2");
	$xpath->registerNamespace('xsi', "http://www.w3.org/2001/XMLSchema-instance");
	$xpath->registerNamespace('xsl', "http://www.w3.org/1999/XSL/Transform");

	return $xpath;

}


function processDOM($content,$updatevals,$deletevals,$insertvals) {


			// Load the XML from a file.
			$dom = new DOMDocument();
			$dom->preserveWhiteSpace = true;
			$dom->LoadXML($content);

			
			foreach ($insertvals as $key => $value) {
	
				$xpath=getXpath($dom);
	
	
				preg_match('/(\d)-(.*\/.*\[\d{1,2}\])/',$key,$matches);
			
				// var_dump($matches);
				// exit;
			
				$firstkeywordnl = $xpath->query($matches[2]."/gmd:MD_Keywords/gmd:keyword[1]");
				$insertorder = $matches[1];
				
				$firstkeyword = $firstkeywordnl->item(0);
				
				$child = $dom->createElement('gmd:keyword');
				$child->appendChild( $dom->createElement('gco:CharacterString',$value));
				
				
				$firstkeyword->parentNode->insertBefore($child,$firstkeyword);
			
			}
			

			foreach ($updatevals as $key => $value) {
				// Locate the value for the first Item Description field.
			
				
				// Create an XPath query.
				// Note: you must define the namespace if the XML document has defined namespaces.
				$xpath=getXpath($dom);
	
				$nodeList = $xpath->query($key);

				if ( $nodeList->length > 0 ) {
					$nodeList->item(0)->nodeValue = $value;
				} 
				elseif ( $value != NULL or $value != "" ) { // must create element
					
					$endloop=false;
					
					$parentxpath=$key;
					$createelemns = array();
					
					//go back the xpath until we find an existing element
					while (! $endloop) {
					
						$temp = explode('/',$parentxpath) ;
						$createelemns[]=array_pop($temp);
						$parentxpath =  implode("/",$temp);

						$xpath=getXpath($dom);
						$parentnl = $xpath->query($parentxpath);
				
						//echo "checking $parentxpath..";
						//echo $dom->saveXML();
				
						if ( $parentnl->length > 0 ) {
							// echo "found \n";
							$endloop=true;
						}
						if ( count($temp) == 1 ) {
							return;
						}
					}
					
					//now create all the missing elements along the path
					$parent = $parentnl->item(0);
					foreach ( array_reverse($createelemns) as $elemname ) {
					
						$child = $dom->createElement($elemname);
						if ($parent->hasChildNodes() ) {
							$parent->insertBefore($child,$parent->firstChild);
						} else {
							$parent->appendChild($child);
						}
						
						$parent=$child;
					
					}
					
					// finally, set the value.. parent now points to the last created element
					$parent->nodeValue = $value;
					
					// re-load dom.. (DOM or xpath stupidity..)
					
					$tmp = $dom->saveXML();
					
					$dom = new DOMDocument();
					$dom->preserveWhiteSpace = true;
					$dom->LoadXML($tmp);

				}
				
			}
			
			$delnodes = array();
			foreach ($deletevals as $key => $value) {
			
				$xpath = getXpath($dom);
				
				preg_match('/(.*)\/(.*\[\d{1,2}\])\//',$key,$matches);
				
				
				$parentnl = $xpath->query($matches[1]);
				$nodenl = $xpath->query($matches[1]."/".$matches[2]);
				
				array_push($delnodes,array( "parent" => $parentnl->item(0) , "child" => $nodenl->item(0)  ) );
								
			}
			
			
			foreach( $delnodes as $pair ) {
				$pair["parent"]->removeChild($pair["child"]);
			}
			
			return $dom->saveXML();

}

function replacewithxpath($mode,$dir,$fileid,$updatevals,$deletevals=array(),$insertvals=array()) {

	$error = false;


	if ( $mode != "db" ) {
	
		if ( count($updatevals) > 0 || count($deletevals) > 0 || count($insertvals) > 0 ) {


			// add datestamp, because we need to always update it

			$updatevals['/gmd:MD_Metadata/gmd:dateStamp/gco:DateTime'] = date("c");

			//var_dump($updatevals);
			//exit;

			$files = array();
			if ($fileid == "*") {
				if ($handle = opendir("$dir")) {
    					while (false !== ($filename = readdir($handle))) {

						$ext = pathinfo($filename, PATHINFO_EXTENSION);
						if ($ext == "xml" ) {
							array_push($files,"$dir/$filename");
						}
					}
				
				}
			} 
			else {
				$fileid = urlencode($fileid);
				array_push($files,"$dir/$fileid.xml");
			}

			foreach ( $files as $filename ) {			
			
					$fh = fopen($filename, 'r');					
					$content = fread($fh, filesize($filename));
					fclose($fh);
					$content = processDOM($content,$updatevals,$deletevals,$insertvals);

					// if the fileidentifier is changed, also change the filename accordingly
					// need to delete old file and write into new one, based on changed fileidentifier
					if ( isset($updatevals['/gmd:MD_Metadata/gmd:fileIdentifier/gco:CharacterString']) and $updatevals['/gmd:MD_Metadata/gmd:fileIdentifier/gco:CharacterString'] != null ) {
										
						$pathinfo = pathinfo($filename);
						$newfilename = $pathinfo["dirname"] . "/" . urlencode($updatevals['/gmd:MD_Metadata/gmd:fileIdentifier/gco:CharacterString']) . ".xml"; 
						unlink($filename);
						
						$filename = $newfilename;
					}
					
					$fh = fopen($filename, "w");
					fwrite($fh,$content);
					fclose($fh);
    			}
		}
	}
	else { // dbmode


		if ( count($updatevals) > 0 || count($deletevals) > 0 || count($insertvals) > 0 ) {

			$files = array();
			if ($fileid == "*") {
				$files = getfiles($mode,$dir);
			} 
			else {
				$fileid = urlencode($fileid);
				array_push($files,$fileid);
			}

			foreach ( $files as $filename ) {
					$content = getfile($mode,$dir,$filename);
					$content = processDOM($content,$updatevals,$deletevals,$insertvals);
					putfile($mode,$dir,$filename,$content);
    			}
		}



	}
	return $error;
	
}


?>
