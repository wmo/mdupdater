<?php
session_start();

include_once("passwd.php");
include_once("lib.php");

if ( isset($_REQUEST["mode"]) ) {
	$mode = $_REQUEST["mode"];
}
else {
	$mode = "list";
}

//$message = "";
$message =  isset($_REQUEST["message"]) ? urldecode($_REQUEST["message"]) : "";
$errormessage =  isset($_REQUEST["errormessage"]) ? urldecode($_REQUEST["errormessage"]) : "";

if ($mode == "login" )  {

	$username = $_REQUEST["username"];
	$password = $_REQUEST["password"];

	if ( isset($userinfo[$username]) && $userinfo[$username]["password"] == $password ) {
	
		$_SESSION["mydir"] = $userinfo[$username]["dir"];
	
	} 
	else {
		$message = "login failed";
		session_destroy();
	}
	

}
if ($mode == "logout" )  {
	session_destroy();
	header("Refresh: 0; url=index.php");
}


if (! isset($_SESSION["mydir"]) ) {
	
	define('SMARTY_DIR', './libs/');
	require_once(SMARTY_DIR . 'Smarty.class.php');
	$smarty = new Smarty();
	@$smarty->display('template_smarty/login.html');
	exit;
}

$dir =  $metadataroot . $_SESSION["mydir"];

if ($mode == "createmdrecord") {


	$filename = $_REQUEST["filename"];
	$template = $_REQUEST["template"];

	if (  checkFilename($filename) ) {
	
	
		$origfilename=$filename;
		
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		if ( $ext != ".xml" ) {
			$filename = "$filename.xml";
		}

		$filename = urlencode($filename);

		$templatefile = "./mdtemplates/$template";
		if(!file_exists($templatefile)) {
			exit("error creating  $templatefile as $filename");
		}

		$fh = fopen($templatefile, 'r');
		$theData = fread($fh, filesize($templatefile));
		fclose($fh);

		putfile($config_filemode,$dir,$filename,$theData);

		$updatevals = array();

		$updatevals['/gmd:MD_Metadata/gmd:fileIdentifier/gco:CharacterString'] = $origfilename;

		$error2 = replacewithxpath($config_filemode,$dir,$origfilename,$updatevals);

		
		$host  = $_SERVER['HTTP_HOST'];
		$uri   = $_SERVER['PHP_SELF'];
		
		header("Location: http://$host$uri?mode=list");
		exit;
	}
	else {
	
		$errormessage = "filename not correct";
	
	}

}
if ($mode == "getfile" ) {

	$filename = urlencode($_REQUEST["filename"]);

	$content = getfile($config_filemode,$dir,$filename);

	# LOAD XML FILE
	$XML = new DOMDocument();
	$XML->loadXML( $content );
	
	$stylesheet = outputxsl();

	# START XSLT
	$xslt = new XSLTProcessor();
	$XSL = new DOMDocument();
	$XSL->loadXML( $stylesheet );
	$xslt->importStylesheet( $XSL );
	#PRINT
	print $xslt->transformToXML( $XML ); 

} 
elseif ($mode == "downloadfile" ) {

	$fileid = $_REQUEST["fileid"];
	$filename = urlencode($fileid.".xml");

	$content = getfile($config_filemode,$dir,$filename);
	
    header('Content-Description: File Transfer');
    header('Content-Type: text/xml');
    header('Content-Disposition: attachment; filename='.$fileid);
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    ob_clean();
    flush();
    echo $content;
    exit;
	
}
elseif ($mode == "uploadfile") {

	if ( $_FILES['uploadedfile']['tmp_name'] ) {
	
		$filename= $_FILES['uploadedfile']['tmp_name'] ;
		$file_info = pathinfo($_FILES['uploadedfile']['name']);
		$message="";
		
		if ($file_info["extension"] == "zip") {
			$zip = new ZipArchive;
			if ($zip->open($filename) === true) {
				for($i = 0; $i < $zip->numFiles; $i++) {
					$entry = $zip->getNameIndex($i);
					if (! checkFilename($entry) ) {
						$errormessage="$entry is not a valid filename";
						continue;
					}
					if (file_exists("$dir/$entry")) 
						continue;
					if ( ! strpos($entry,"/") === false )
						continue;
					$zip->extractTo($dir);
				}
			} 
			$zip->close();
			if ($errormessage=="") {
				$message="uploaded zip file";
			}
		} elseif ( $file_info["extension"] == "xml" ) {
		
			if (checkFilename($file_info["filename"])) {
				rename($filename,"$dir/".$file_info["filename"]);
				$message="uploaded single file";
			} else {
				$errormessage=$file_info["filename"]." not a valid filename";
			}
		}
		
		header("Refresh: 0; url=index.php?message=".urlencode($message)."&errormessage=".urlencode($errormessage));
    }
    else{
         echo "ZIP archive failed";
    }
	
	
	
}
elseif ($mode == "downloadzip") {

	$zipfile = tempnam(sys_get_temp_dir(), 'Tux');
	$zip = new ZipArchive;
	$zip->open($zipfile, ZipArchive::CREATE);

	$files = getfiles($config_filemode,$dir);
	
	foreach($files as $file) {
		$content = getfile($config_filemode,$dir,$file);
	 	$zip->addFromString("$dir/$file",$content);
    	}

	$zip->close();

	header('Content-Type: application/zip');
	header("Content-disposition: attachment; filename=all.zip");
	header('Content-Length: ' . filesize($zipfile));
	readfile($zipfile);
	unlink($zipfile);

}
else {


define('SMARTY_DIR', './libs/');
require_once(SMARTY_DIR . 'Smarty.class.php');
$smarty = new Smarty();

if ($mode == "delete") {

	$deletefiles = $_REQUEST["deletefiles"];
	$message = "";
	
	
	$errors = deletefiles($config_filemode,$dir,$deletefiles);

	if (count($errors)>0 ) {
	
		$errormessage = "error deleting files</br>";
		
		foreach ($errors as $errors) {
			$errormessage .= "problem with : ".$error."</br>";
		}
		$smarty->assign("errormessage",$errormessage);
	}
	
	if ( count($deletefiles) - count($errors) > 0 ) {
	
		$message = "sucesfully deleted ". (count($deletefiles) - count($errors))." records..";
		$message .= "<ul>";
	
		foreach ($deletefiles as $file) {
			if (! array_key_exists($file,$errors)) {
				$message .= "<li>".urldecode($file)."</li>";
			}
		}
		$message .= "</ul>";
	
	
		$smarty->assign("message",$message);
	}
	
}



if ($handle = opendir('./mdtemplates')) {
    $templates = array();
    while (false !== ($file = readdir($handle))) {
       if ( $file == "." or $file == ".." ) {
	    continue ;
	   }
	   
	   array_push($templates,basename($file));
    }
    closedir($handle);

    $smarty->assign("templates",$templates);
}

$files = getfiles($config_filemode,$dir);
$smarty->assign("files",$files);
$smarty->assign("message",$message);
$smarty->assign("errormessage",$errormessage);

@$smarty->display('template_smarty/listfiles.html');


}


?>
