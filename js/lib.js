var map = null;
var dragging = false;
var previousLoc = null;
var resizedragging = false;
var resizepreviousLoc = null;
var insertnum=1;


function encodexpath($string) {
	return $string.replace(/\[/g,"%5b").replace(/\]/g,"%5d");
}

function GetMap(north,east,south,west)
         {
		 
		 
			
						
			var center = new Microsoft.Maps.Location( (north + south) / 2 ,  (west + east) / 2  );
			
			
            map = new Microsoft.Maps.Map(document.getElementById("mapDiv"),
			{
			credentials:"AlYJXjZ-C6vxykikMAYcm7khkrTKQcZ-YXQcznb3Lh7TJeNH6qM75BM5x7YuUKEp",
			
			enableSearchLogo: false,
			enableClickableLogo: false,
			showMapTypeSelector:false,
			showDashboard: false,
			
			}); 

       		
				           var points = [
							new Microsoft.Maps.Location(north,west ),
							new Microsoft.Maps.Location(north,east),
							new Microsoft.Maps.Location(south,east),
							new Microsoft.Maps.Location(south,west ),
							new Microsoft.Maps.Location(north,west )      
							];
							
			
		
				
				
            var polygonStrokeColor = new Microsoft.Maps.Color(100, 0, 0, 200);
			var polygonFillColor = new Microsoft.Maps.Color(100, 0, 100, 0);
			
            var MyPolygon = new Microsoft.Maps.Polygon(points, { fillColor: polygonFillColor, strokeColor: polygonStrokeColor, strokeThickness: 2 });
			
						
			map.entities.push(MyPolygon);
			
			var bestview = Microsoft.Maps.LocationRect.fromLocations(points.slice(0,4));
			
			map.setView( {bounds:bestview} );
			
			
			Microsoft.Maps.Events.addHandler(MyPolygon, 'mousedown', StartDragHandler);
			Microsoft.Maps.Events.addHandler(map, 'mousemove', DragHandler);
			Microsoft.Maps.Events.addHandler(MyPolygon, 'mouseup', EndDragHandler);
			Microsoft.Maps.Events.addHandler(MyPolygon, 'mouseout', EndDragHandler);
			
			
						
         }
		 


//mousedown handler
function StartDragHandler(e) {

	
	var currentPos = map.tryPixelToLocation(new Microsoft.Maps.Point(e.getX(), e.getY()));
	var lowerRightCorner = e.target.getLocations()[2];

	if ( Math.abs(currentPos.longitude -  lowerRightCorner.longitude ) < 1 && Math.abs(lowerRightCorner.latitude - currentPos.latitude) < 1) {
	
			resizedragging = true;
			resizepreviousLoc = map.tryPixelToLocation(new Microsoft.Maps.Point(e.getX(), e.getY()));
	}
	else {
	    dragging = true;
		previousLoc = map.tryPixelToLocation(new Microsoft.Maps.Point(e.getX(), e.getY()));
	}
}

//mousemove handler
function DragHandler(e) {

	var mapElem = map.getRootElement();
	
	if (e.targetType == "polygon") {
		var currentPos = map.tryPixelToLocation(new Microsoft.Maps.Point(e.getX(), e.getY()));
		var lowerRightCorner = e.target.getLocations()[2];
		
	
		if ( !dragging && !resizedragging &&  Math.abs(currentPos.longitude -  lowerRightCorner.longitude ) < 1 && Math.abs(lowerRightCorner.latitude - currentPos.latitude) < 1) {
		
			mapElem.style.cursor = "se-resize";

		} 
		else if (resizedragging || dragging) {
		}
		else {
		    mapElem.style.cursor = "move";
		}
	} else {
	
		mapElem.style.cursor = "pointer";
	
	}

    if (dragging) {
        var loc = map.tryPixelToLocation(new Microsoft.Maps.Point(e.getX(), e.getY()));
        var latVariance = 0;
        var longVariance = 0;
        
        //determine variance
        latVariance = loc.latitude - previousLoc.latitude;
        longVariance = loc.longitude - previousLoc.longitude;
        
        previousLoc = loc;
        
        //adjust points in current shape
        var currentPoints = e.target.getLocations();
        for (var i = 0; i < currentPoints.length; i++) {
            currentPoints[i].latitude += latVariance;
            currentPoints[i].longitude += longVariance;
        }
        
        //set new points for polygon
        e.target.setLocations(currentPoints);
        
        e.handled = true;
    } 
	else if (resizedragging) {
		
		var loc = map.tryPixelToLocation(new Microsoft.Maps.Point(e.getX(), e.getY()));
        var latVariance = 0;
        var longVariance = 0;
        
        //determine variance
        latVariance = loc.latitude - resizepreviousLoc.latitude;
        longVariance = loc.longitude - resizepreviousLoc.longitude;
        
        resizepreviousLoc = loc;
        
        //adjust points in current shape
        var currentPoints = e.target.getLocations();
		
		
		
        currentPoints[1].longitude += longVariance;
        currentPoints[2].latitude += latVariance;
        currentPoints[2].longitude += longVariance;
        currentPoints[3].latitude += latVariance;
        
		
        //set new points for polygon
        e.target.setLocations(currentPoints);
	
		e.handled = true;
	}
}

//mouseup & mouseout handler
function EndDragHandler(e) {
    dragging = false;
	resizedragging = false;
	
	updateCoordinates(e.target.getLocations());
}		 

function updateCoordinates(points) {

	var west = document.getElementsByName("update-/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:extent/gmd:EX_Extent/gmd:geographicElement/gmd:EX_GeographicBoundingBox/gmd:westBoundLongitude/gco:Decimal");
	var east = document.getElementsByName("update-/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:extent/gmd:EX_Extent/gmd:geographicElement/gmd:EX_GeographicBoundingBox/gmd:eastBoundLongitude/gco:Decimal");
	var north = document.getElementsByName("update-/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:extent/gmd:EX_Extent/gmd:geographicElement/gmd:EX_GeographicBoundingBox/gmd:northBoundLatitude/gco:Decimal");
	var south = document.getElementsByName("update-/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:extent/gmd:EX_Extent/gmd:geographicElement/gmd:EX_GeographicBoundingBox/gmd:southBoundLatitude/gco:Decimal");	
	
	if (  west.length != 0 &&  east.length != 0 && north.length != 0 && south.length != 0 ) {
		
		var northval = Math.max( points[0].latitude, points[2].latitude );
		var southval = Math.min( points[0].latitude, points[2].latitude );
		var eastval = Math.max( points[0].longitude, points[1].longitude );
		var westval = Math.min( points[0].longitude, points[1].longitude );

		
		west[0].value = westval;
		east[0].value = eastval;
		north[0].value = northval;
		south[0].value = southval;
		
		console.log(west);
		console.log(east[0]);
		
		
	}
}


function SelectMap() {

//Wire events for dragging

	

}