

$(document).ready(function(){

	//Hide (Collapse) the toggle containers on load
	$("#uploadform").hide(); 
	$("#createform").hide(); 
	

	//Switch the "Open" and "Close" state per click then slide up/down (depending on open/close state)
	$("a.triggerupload").click(function(){
		
		$("#uploadform").toggle();

		return false; //Prevent the browser jump to the link anchor
	});
	
		//Switch the "Open" and "Close" state per click then slide up/down (depending on open/close state)
	$("a.triggercreate").click(function(){
		
		$("#createform").toggle();

		return false; //Prevent the browser jump to the link anchor
	});

});