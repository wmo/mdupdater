

$(document).ready(function(){

	//Hide (Collapse) the toggle containers on load
	$(".toggle_container").hide(); 
	$(".triggeredit").hide(); 
	$(".updateall").hide(); 
	
	

	//Switch the "Open" and "Close" state per click then slide up/down (depending on open/close state)
	$("a.trigger").click(function(){
		$(this).parent().next().slideToggle("slow");
		
		// don't show the edit button when closing the widget when we are editing already
		//console.log($(this).next(".triggeredit").attr("editenabled"));
		if ( $(this).next(".triggeredit").attr("editenabled") != 1 ) {
			$(this).next(".triggeredit").toggle();
		}
	
		if ($(this).parent().next().find("#mapDiv").length > 0 ) {
		
			var north = parseFloat($("#north").text());
			var south = parseFloat($("#south").text());
			var east =  parseFloat($("#east").text());
			var west =  parseFloat($("#west").text());
			
			
		           // Create the locations
	
			GetMap(north,east,south,west);
		}
		
		return false; //Prevent the browser jump to the link anchor
	});
	
	$("a.geoclass").click(function(){
		SelectMap();
	});
	
	$("a.triggeredit").click(function(){
		
		
		//console.log($(this).parent().next().find("table.fields").find("tr.valrow"));
		
		$(this).parent().next().find("table.fields").find("tr.valrow").each(
			function(index) {
			
			 $formelem = $(this).children("td:last").children("span:first");
			 $valelem = $(this).children("td:first").children("span:last");
			 $type = $valelem.attr("type");
			 $prio = $valelem.attr("prio");
			 $xpath = encodexpath($valelem.attr("xpath"));

			 if ( $type == "dropdown" ) {
					$value = $valelem.text();
					$input = '<select name="update-'+$prio+'-'+$xpath+'">';
						var myarray = eval($valelem.attr("valid")+"values");
						for (var i in myarray) {
							var temp;
							if ( $value == myarray[i] ) {	temp = 'selected="selected"'; } 
							else {	temp = "" ;  }
							$input += "<option "+temp+">"+myarray[i]+"</option>";
						}
					$input += '</select>';
			 }
			 else {
			 
				 if ( $valelem.text().length > 50 ) {
			 	 	$input = '<textarea class="valinput" cols="50" name="update-'+$prio+'-'+$xpath+'" >'+$valelem.text()+'</textarea>'
			 	} else {
			 	 	$input = '<input class="valinput" type="text" name="update-'+$prio+'-'+$xpath+'" value="'+$valelem.text()+'" />'
			 	}
			 }
			 $formelem.append($input);
			 }
			 
		
		);
		
		$(this).parent().next().find("span.updateall:first").show();
		
		$(this).attr("editenabled",1);
		
		$(this).hide();
		
		updateValidation();

		
		return false; //Prevent the browser jump to the link anchor
	});
	

	
	
	$("a.triggereditdelete").click(function(){
		
		//console.log($(this).parent().next().find("table.fields").find("tr.valrow"));
		
		$(this).parent().next().find("table.fields").find("th").each(
			function(index) {
			
			$(this).children("p:first").append('<a style="padding-left:5px;" href="#" class="addkeyword">add keyword</a>');
			
			$(this).find("a.addkeyword:first").bind('click',function(){
	
				console.log($(this).parents("tr"));
				
				var xpath = $(this).parent("p.thesaurustitle").attr("xpath");
				
				$(this).parents("tr:first").after('<tr class="valrow" ><td>new value</td><td><input class="valinput" type="text" value="new keyword" name="insert-'+insertnum+'-'+encodexpath(xpath)+'" /></td></tr>');
				
				
				insertnum++;
				
				return false;
			});
			
			
			}
		
		);
		
		$(this).parent().next().find("table.fields").find("tr.valrow").each(
			function(index) {
			
			 $formelem = $(this).children("td:last").children("span:first");
			 $valelem = $(this).children("td:first").children("span:last");
			 
		 	 $input = '<input type="checkbox" class="delbox" name="delete-'+encodexpath($valelem.attr("xpath"))+'" />';
			
			 $formelem.append($input);
			
			}
		
		);
		
		$(this).parent().next().find("span.updateall:first").show();
		
		$(this).hide();
		

		return false; //Prevent the browser jump to the link anchor
	});
	
	
	
	$(".updateallbox").click(function(){
	
		if ( $(this).is(":checked") ) {
			$(this).parents(".toggle_container").css("background","red");
		} else {
			$(this).parents(".toggle_container").css("background","grey");
		}
	
	});
	
	
	$(".open_beginning").show();

});
