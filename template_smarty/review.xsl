<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
		xmlns:gmd="http://www.isotc211.org/2005/gmd" 
		xmlns:gmx="http://www.isotc211.org/2005/gmx" 
		xmlns:gco="http://www.isotc211.org/2005/gco" 
		xmlns:xlink="http://www.w3.org/1999/xlink" 
		xmlns:gts="http://www.isotc211.org/2005/gts" 
		xmlns:gml="http://www.opengis.net/gml/3.2" 
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		>

	<xsl:output
			method="html"
			omit-xml-declaration="yes"
			doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
			doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
			indent="yes"/>

	<xsl:template match="/gmd:MD_Metadata">

		{function name="generatefieldscontact"}
					<h2>{$description}<a  class="trigger" href="#">toggle view</a><a class="triggeredit" href="#">edit</a></h2>
					<div class="toggle_container">
						<div class="block">
							<span class="updateall">Update in all files: <input type="checkbox" class="updateallbox" name="{$sectionname}_updateall" /> </span>

							<table class="fields">
								{foreach from=$section key=k item=v}
								<tr class="valrow">
									<td>
										<span class="keyword"> {$v.name}:</span>
										<span xpath="{$v.xpath}" prio="{$v.prio}" class="fieldvalue">
											<xsl:value-of select="{$v.xpath}"/>
										</span>
									</td>

									<td>

										<span class="editform">
										</span>
									</td>
								</tr>
								{/foreach}

							</table>
						</div>
					</div>
		{/function}

		{function name="generatefields" cssclass="triggeredit" cssclass2="toggle_container"}
					<h2>{$description}<a  class="trigger" href="#">toggle view</a><a class="{$cssclass}" href="#">edit</a></h2>
					<div class="{$cssclass2}">
						<div class="block">

							<table class="fields">
								{foreach from=$section key=k item=v}
								<tr class="valrow">
									<td>
										<span class="keyword"> {$v.name}:</span>
										<span xpath="{$v.xpath}" prio="{$v.prio}" class="fieldvalue" {if isset($v.type)} type="{$v.type}" valid="{$v.id}"{/if}>
											<xsl:value-of select="{$v.xpath}"/>
										</span>


									</td>

									<td>
										<span class="editform">
										</span>
									</td>
								</tr>
								{/foreach}

							</table>
						</div>
					</div>
		{/function}


		<html>
			<head>

				<script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0"></script>



		
				<script type="text/javascript" src="js/jquery.js"></script>
				<script type="text/javascript" src="js/jquery.validate.min.js"></script>
				<script type="text/javascript" src="js/custom.js"></script>
				<script type="text/javascript" src="js/lib.js"></script>


				<script type="text/javascript">
				{$data = [ $constraintsection,$filenamesection ]}
				{foreach $data as $value}
					{foreach from=$value key=k item=v}
						{if isset($v.type)} 
							{if $v.type == "dropdown"}
								{$v.id}values = new Array(
									{foreach $v.values as $value} "{$value}" {if $value@last}{else},{/if}{/foreach}
								);
							{/if}
						{/if}

					{/foreach}
				{/foreach}
				
				
				function updateValidation() {
				
					$( "#mdform" ).validate({
						rules: {
						
							{foreach from=$validationrules key=id item=rule name=rules}
						
							"{$rule.fieldname}" : {
								required: true,
								{$rule.type} : {$rule.value}
							}
							
							{if $smarty.foreach.rules.last}{else},{/if}
							
							{/foreach}
						}
					});
				
				}

				
				// {$validations}
				
				</script>
					
				<link rel="stylesheet" type="text/css" href="css/css.css" />
			</head>

			<body>

				<div class="infobox">
					<h1>WMO WIS metadataeditor</h1>
					<p>Please review the information below. You can open a section by clicking on the "toggle view" link. Clicking on "edit" enables the edit fields. Some sections, such as the contact one, are configured to allow updating all the files in the directory with the information input.
					You can download the original file by clicking on the download link.
					</p>
				</div>

				<form method="POST" id="mdform" action="update.php">
					<input type="hidden" name="fileid" > 
						<xsl:attribute name="value">
							<xsl:value-of select="{$fileid}"/>
						</xsl:attribute>
					</input>



					<p><a href="?mode=list">go to parent directory</a></p>
					<p><a>
						<xsl:attribute name="href">
							<xsl:value-of select="concat('?mode=downloadfile&amp;fileid=',{$fileid})"/>
						</xsl:attribute>
						download file
					</a></p>
					


					{generatefields section=$filenamesection description="General Info" cssclass="triggeredit open_beginning" cssclass2="toggle_container open_beginning"}
					{generatefields section=$constraintsection description="Bulletin constraints"}


					<h2>Geographical extent<a  class="trigger" href="#">toggle view</a><a class="triggeredit geoclass" href="#">edit</a></h2>
					<div class="toggle_container">
						<div class="block">


							<table>
								<tr >

									<td>
										<table class="fields">
											{foreach from=$extentsection key=k item=v}
											<tr class="valrow">
												<td>
													<span class="keyword"> {$v.name}:</span>
													<span id="{$v.name}" prio="{$v.prio}" xpath="{$v.xpath}" class="fieldvalue">
														<xsl:value-of select="{$v.xpath}"/>
													</span>
												</td>

												<td>

													<span class="editform">
													</span>
												</td>
											</tr>
											{/foreach}

										</table>
									</td>
									<td>

										<div id='mapDiv' style="position:relative; width:400px; height:400px; margin-left:60px;"></div>       

									</td>
								</tr>
							</table>
						</div>
					</div>


					<h2>Keywords <a  class="trigger" href="#">toggle view</a><a class=" triggereditdelete triggeredit" href="#">edit</a></h2>
					<div class="toggle_container">
						<div class="block">


							<xsl:for-each select="{$desckeywords}">
								<xsl:variable name="i" select="position()"/>

								<table  class="fields">
									<tr><th colspan="2">
											<p class="thesaurustitle">
													<xsl:attribute name="xpath">
														<xsl:value-of select="concat('{$desckeywords}[',$i,']')"/>
													</xsl:attribute>
												<xsl:value-of select="{$relativ_thesaurus}">
												</xsl:value-of>

											</p>
										</th></tr>


									<xsl:for-each select="{$relativ_thes_keyword}">
										<xsl:variable name="j" select="position()"/>

										<tr class="valrow">
											
											<td width="250px;">
												<span></span>
												<span class="fieldvalue">
													<xsl:attribute name="xpath">
														<xsl:value-of select="concat('{$desckeywords}[',$i,']/{$relativ_thes_keyword}[',$j,']/gco:CharacterString')"/>
													</xsl:attribute>

													<xsl:value-of select="gco:CharacterString"/>
												</span>
											</td>

											<td ><span class="editform"></span>
											</td>

										</tr>

									</xsl:for-each>


								</table>

							</xsl:for-each>





						</div>
					</div>


					{generatefieldscontact description="Contact details for Metadata" section=$mdcontactsection sectionname="mdcontactsection"}
					{generatefieldscontact description="Contact details for Data" section=$contactsection sectionname="contactsection"}
					{generatefieldscontact description="Distributor details and format" section=$distributorsection sectionname="distribcontact"}



					<input style="margin-top:20px;" type="submit" value="update record"/>
				</form>

			</body></html>

	</xsl:template>




</xsl:stylesheet>
